/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.selfmention;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

/**
 * Responds with a short message when the bot is mentioned.
 * @author Leon Kappes / Lee, traxam
 */
public class SelfMentionListener extends ListenerAdapter {
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (!event.getAuthor().isBot()
                && !event.getAuthor().isFake()
                && event.getMessage().isMentioned(event.getJDA().getSelfUser())
                && event.getMessage().getContentRaw().equals(String.format("<@%s>", event.getJDA().getSelfUser().getId())))
            event.getChannel().sendMessage(
                    new EmbedBuilder()
                            .setAuthor("Rubicon", null, event.getJDA().getSelfUser().getEffectiveAvatarUrl())
                            .setDescription("Hi, I am Rubicon and I'm here to help you! " +
                                    "Type `rc!help` to find out how you can use me. " +
                                    "If you need any help, feel free to join [my support server](http://server.rucb.co/). :)")
                            .addField("Useful Links",
                                    "[Visit my Website](https://rubicon.fun/) \u2022 " +
                                            "[View my Documentation](https://rubicon.fun/docs/) \u2022 " +
                                            "[Invite me](https://inv.rucb.co/) \u2022 " +
                                            "[Join my support server](https://server.rucb.co/)", true)
                            .build()
            ).queue();

    }
}
