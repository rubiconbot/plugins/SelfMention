package fun.rubicon.selfmention;

import fun.rubicon.plugin.Plugin;

/**
 * @author Leon Kappes / Lee
 * @copyright RubiconBot Dev Team 2018
 * @License GPL-3.0 License <http://rubicon.fun/license>
 */
public class SelfMention extends Plugin {

    @Override
    public void init() {
        this.registerListener(new SelfMentionListener());
    }
}
